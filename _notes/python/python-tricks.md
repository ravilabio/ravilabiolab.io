---
layout: notes
title: Python Tricks
aside:
  toc: true
sidebar:
  nav: notes-nav
---

<!-- vim-markdown-toc GitLab -->

* [Imports](#imports)

<!-- vim-markdown-toc -->

# Imports

Add a folder to path. Useful to call a module that is in a parent directory:

```python
import sys
sys.path.append("../")
```
