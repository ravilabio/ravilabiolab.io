---
layout: article
title: About
excerpt: Ricardo Avila is a bioinformatics nerd, artist, and lover of open source software.
---


<div class="item">
  <div class="item__image">
    <img class="image--lg" style="border-radius: 8px" src="/assets/author.jpg">
  </div>
  <div class="item__content">
    <div class="item__header">
      <h2>About Me</h2>
      <p>Hi! 👋 My name is Ricardo Avila. I enjoy researching computational problems in biology, ranging from protein structures, to genomic sequences and chemical structures. I find that the most difficult and rewarding problems often require connecting seemingly unrelated fields. Hence, I enjoy being in the overlap of different disciplines, figuring out how things come together.</p>
    </div>
  </div>
</div>

I grew up in the sunny city of El Paso, Texas, and graduated from the University of Texas at El Paso (UTEP), where I studied a Bachelor's of Science in Biochemistry, and a Master's degree in Bioinformatics. I am currently a research programmer at Scripps, in San Diego, CA, where I work in making biological data more [FAIR](https://en.wikipedia.org/wiki/FAIR_data) (findable, accesible, interoperable, and reusable) for the scientific community.

In my free time I like drawing, taking photographs, reading science fiction, and playing music. For more of that, you can visit my second homepage:
[ravilart.com](https://ravilart.com){:.button.button--primary.button--pill}

## About This Site

This blog has seen several iterations in my attempts at finding the best format for writing. I needed something that can easily display code, documentation and mathematical formulas. Therefore, I have settled on writing these posts in markdown using Jekyll, and hosting the source code on GitLab.


In this small blog, I plan to make regular posts about bioinformatics projects, and miscellaneous code snippets. Most of the time I am coding in Python, R, or BASH... but I will post about whatever I am learning at the moment. My main motivation is that I frequently find myself trying to remember the solution to an old problem that I know I solved before, but spending a long time trying to remember which project it came up in. Hence, I have decided that blogging would be a good habit to build. I am hoping that some of these posts can be helpful to other people as well.

## Contact

I'm friendly! If you have any questions or comments, feel free to shoot me an email.

📧 ravila@protonmail.com

[🔑️ PGP Encryption Key](/assets/publickey.pgp){:.button.button--primary.button--rounded.button--lg}
