---
title: "Mapping Pharos Targets to PDB Structures"
excerpt: "The SIFTS (Structure Integration with Function, Taxonomy and Sequence) database provides mappings  between UniProt genes and PDB structures, among other things.
Using these mappings, we count the number of targets from the NIH's Pharos database
which have ligands with known binding affinities, and at least one structure in the Protein Data Bank."
tags:
    - Python
    - SIFTS
    - PDB
    - Pharos
---

- [Problem Description](#problem-description)
- [Getting the Data](#getting-the-data)
- [Read SIFTS Mappings](#read-sifts-mappings)
- [Find PDB IDs](#find-pdb-ids)
- [Summarizing the Data](#summarizing-the-data)
- [Visualizing the Data](#visualizing-the-data)

## Problem Description

 <img src="/assets/images/SIFTS.png" style="width:500px"> 
 
The Structure Integration with Function, Taxonomy and Sequence ([SIFTS](https://www.ebi.ac.uk/pdbe/docs/sifts/)) database
provides mappings between UniProt and  PDB, as well as annotations from GO, InterPro, Pfam, CATH, SCOP, PubMed, Ensembl and other resources.
Here, we map all the receptors from the [Pharos](https://pharos.nih.gov/idg/index) database to their PDB IDs,
using their UniProt accession numbers.

__The goal is to obtain a dataset of human targets with available structures and known ligand binding affinities.__
I also want to get the distribution of these PDB structures across different receptor families,
such as Kinases, GPCRs, Ion Channels, Nuclear Receptors, and Transporters.

## Getting the Data
 
First we read in Pharos data csv files downloaded from Pharos for targets in the **Tclin** (targets with approved drugs),
and  **Tchem** (targets with known binding affinities), for several receptor classes.
The csv files contain UniProt IDs for each receptor. All downloaded data and code is available in my GitHub repository:
[ravila4/Pharos-to-PDB](https://github.com/ravila4/Pharos-to-PDB)


{% highlight python %}
import pandas as pd

target_classes = ["GPCRs", "ion-channels", "kinases", "nuclear-receptors", "transporters"]
IDG_data = {}
for tclass in target_classes:
    IDG_data[tclass] = pd.read_csv("data/" + tclass + ".csv", index_col=False)
{% endhighlight %}
 
## Read SIFTS Mappings

The mappings were downloaded as a CSV file from 
[their ftp site](ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/csv).

{% highlight python %}
uniprot_to_pdb = pd.read_csv("data/uniprot_pdb.csv", skiprows=1)
uniprot_to_pdb.head()
{% endhighlight %}


<figcaption>A sample of the SIFTS Data Frame.</figcaption>
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: left;
    }
</style>
<table border="0" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th></th>
      <th>SP_PRIMARY</th>
      <th>PDB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>A0A010</td>
      <td>5b00;5b01;5b02;5b03;5b0i;5b0j;5b0k;5b0l;5b0m;5...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>A0A011</td>
      <td>3vk5;3vka;3vkb;3vkc;3vkd</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A0A014C6J9</td>
      <td>6br7</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A0A016UNP9</td>
      <td>2md0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>A0A023GPI4</td>
      <td>2m6j</td>
    </tr>
  </tbody>
</table>
</div>

## Find PDB IDs

Here's a function for joining the two Data Frames:

{% highlight python %}
def find_pdbs(df):
    """ Input: Data Frame of Pharos data.
        Output: List of PDB IDs. """
    IDS = []
    for i in range(len(df)):
        pdb_ids = None
        uniprot_id = df.loc[:, "Uniprot ID"][i]
        mapping = uniprot_to_pdb[uniprot_to_pdb.SP_PRIMARY == uniprot_id]
        if len(mapping) != 0:
            pdb_ids = mapping.PDB.iloc[0].split(';')
        IDS.append(pdb_ids)
    return IDS
{% endhighlight %}
 
Adding PDB IDs to Pharos targets:

{% highlight python %}
for df in IDG_data.values():
    df['PDB_IDS'] = find_pdbs(df)
{% endhighlight %}

## Summarizing the Data
 
Number of receptors in each class with at least one structure in the Protein
Data Bank: 

{% highlight python %}
pdbs_per_class = {}

for IDG_class in IDG_data:
    df = IDG_data[IDG_class]
    num_available = len(df) - sum(df.PDB_IDS.isna())
    pdbs_per_class[IDG_class] = num_available

pdbs_per_class
{% endhighlight %}

`{'GPCRs': 77,
 'ion-channels': 70,
 'kinases': 304,
 'nuclear-receptors': 41,
 'transporters': 15}`

## Visualizing the Data

Finally, we visualize the results with a pie chart:

{% highlight python %}
import matplotlib.pyplot as plt

width=0.3
labels = ["{}: {}".format(f, n) for f, n in zip(pdbs_per_class.keys(),
          pdbs_per_class.values())]
plt.pie(pdbs_per_class.values(), labels=labels, radius=1,
        wedgeprops=dict(width=width, edgecolor='w'))

plt.show()
{% endhighlight %}

 
![svg]({{ BASE_PATH }}/assets/images/2019-2-14-mapping-pharos-targets-to-pdb-structures_19_0.svg) 

