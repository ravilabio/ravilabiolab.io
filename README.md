# Ricardo Avila's Bioinformatics Blog

## Getting Started
1. Clone this repository and `cd` into it
2. Install Jekyll and Bundler: `gem install jekyll bundler`
3. Install required packages: `bundle install --path vendor/bundle`
4. Start server: `bundle exec jekyll serve`

## Creating New Blog Posts

Blog post writing takes place on "authoring" branch.
Blog posts start with the following YAML front matter:

```
---
title: "Post Title"
excerpt: "A short description."
tags:
    - tag1
    - tag2
---
```

The authoring branch contains additional folders not in master:
- **notebooks/** - store original Jupyter notebooks.
- **templates/** - templates for converting Jupyter notebooks to markdown.
- **jekyll.py** - config script for converting Jupyter to markdown.

Blog posts can be drafted using the Netlify CMS webapp:  https://ravilabioinfo.netlify.app/admin/#/

## Creating New Notes

Notes have the following YAML front matter:

```
---
layout: notes
title: "Title"
aside:
  toc: true
sidebar:
  nav: notes-nav
---
```

After creating a new note page, it must be added to the "notes-nav" section of `_data/navigation.yml`.

